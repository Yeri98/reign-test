import React from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from './pages/Home';
import './App.css';

function App() {
  return (
    <Router>
      <Routes>
          <Route  path='/' element={<Home />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
